#@File(label="Select a directory with crop to analyse", style="directory") folderInput
#@File(label='Saving directory', style='directory') saveDir
#@boolean(label="automatic analysis?", value = False) control

from ij import IJ,ImagePlus,WindowManager
from ij.plugin.frame import RoiManager
import os
from ij.plugin import Duplicator
from loci.formats import ImageReader
from loci.formats import MetadataTools
from ij.measure import ResultsTable
from ij.text import TextWindow
import glob
import re
from ij.gui import GenericDialog
from ij.gui import WaitForUserDialog

def natsort(list_):
    # decorate
    tmp = [ (int(re.search('\d+', i).group(0)), i) for i in list_ ]
    tmp.sort()
    # undecorate
    return [ i[1] for i in tmp ]

def find_MicroNuclei(imp):

	IJ.run(imp, "Median...", "radius=1")
	IJ.setAutoThreshold(imp, "Huang dark")
	ip_mask = imp.createThresholdMask()	
	imp_mask = ImagePlus("mask",ip_mask)
	imp_mask.show()
	
	IJ.run(imp_mask, "Fill Holes", "")
	IJ.run(imp_mask, "Watershed", "")
	IJ.run(imp_mask, "Analyze Particles...", "size=1-30 exclude add")
	IJ.run("Threshold...")
	IJ.resetThreshold(imp)
	imp_mask.hide()
	rm = RoiManager.getInstance()
	rm.runCommand(imp,"Show All with labels")
	print(control)
	if (control == False):
		WaitForUserDialog("Check MicroNuclei","Check Micronuclei then click OK").show()
	
	imp_mask.close()
	roi_nuclei = imp_mask.getRoi()
	rm = RoiManager.getInstance()
	num_nuclei = rm.getCount()
	return roi_nuclei, num_nuclei

def run_script():
	rm = RoiManager.getInstance()
	if rm != None :
		rm.close()
	rm = RoiManager()
	
	save_Dir = saveDir.getAbsolutePath()
	if not os.path.exists(save_Dir):
			os.makedirs(save_Dir)
	folder = folderInput.getAbsolutePath()
	file_list = glob.glob1(folder,"*.tif")
	tifCounter = len(file_list)
	file_list = natsort(file_list)
	
	rt_exist = WindowManager.getWindow("MicroNuclei")
	if rt_exist==None or not isinstance(rt_exist, TextWindow):
		rt= ResultsTable()
	else:
		rt = rt_exist.getTextPanel().getOrCreateResultsTable()
	
	dupl = Duplicator()
	
	for i in range(0,tifCounter):
	
		imageFile = file_list[i]
		IJ.run("Bio-Formats", "open=["+os.path.join(folder, imageFile)+"] autoscale color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT")
		imp_orig = IJ.getImage()
		imp_orig.hide()
		title = imp_orig.getTitle()
		imp = dupl.run(imp_orig)
		imp.show()
		roi_nuclei, num_nuclei = find_MicroNuclei(imp)
		rt.incrementCounter()
		rt.addValue("image",title)
		rt.addValue("num_MicroNuclei",num_nuclei)
	
		rt.show("MicroNuclei")
		imp.hide()
		imp.close()
		rm.runCommand(imp_orig,"Show All with labels")
		
		IJ.saveAs(imp_orig, "Tiff", os.path.join(save_Dir, title))
		imp_orig.hide()
		imp_orig.close()
	
		rm = RoiManager.getInstance()
		if rm != None :
			rm.close()
		rm = RoiManager()



			

if __name__ in ['__builtin__', '__main__']:
    run_script();






