#@File (label = "Output directory", style = "directory") dstFile
#@Float(label="dimension of the side of the field (um)", value = 1000) side_len
#@Float(label="starting offset of the grid, negative value to center ", value = 0) offset


'''
Created on Oct 16, 2018

subdivide opened image using an user defined grid and save the crop setting square offset and square dimension

@author: emartini
'''
from java.lang import Math


from ij import IJ,ImageStack, ImagePlus
from ij.plugin import Duplicator, MontageMaker
from ij.gui import GenericDialog
from ij.plugin.frame import RoiManager
from ij.gui import Roi
from ij.plugin.filter import ParticleAnalyzer as PA
from ij.measure import Measurements, ResultsTable
import os
import string
import gc
from ij.gui import WaitForUserDialog 
from ij import WindowManager as WM
from ij.text import TextWindow


def todo_crop(imp,curr_roi,dstDir,side_len,offset):
	if (os.path.isdir(dstDir))==False:
		os.mkdir(dstDir)
	dup = Duplicator().run(imp)
	dup.setTitle(curr_roi.getName())
#	dup.show()
				
	save_path_folder =str(dstDir)+os.sep+imp.getTitle()+'_grid_'+str(side_len)+'_offset_'+offset
	#save_path = format_filename(save_path)
	
	if (os.path.isdir(save_path_folder))==False:
		os.mkdir(save_path_folder)
	save_path = save_path_folder+os.sep+curr_roi.getName()+'_CROPPED.tif'	
	IJ.save(dup, save_path)
	print save_path
	return save_path_folder


def run_script():
	imp = IJ.getImage()
	imp.setActiveChannels("1110");
	dstDir = dstFile.getAbsolutePath()
	analyseSingleImage(imp,dstDir,side_len,offset)
	


def analyseSingleImage(imp,dstDir,side_len,offset):
	rm = RoiManager.getInstance()
	if rm != None :
		rm.close()
	rm = RoiManager()
	IJ.run(imp, "Select None", "");
	IJ.run("Hide Overlay", "");
	dstDir_path = dstDir	
	tit_img = imp.getTitle()
	imp.setTitle(tit_img)
	calib = imp.getCalibration()
	h = imp.getHeight()
	w = imp.getWidth()	
	area_imp = w*h
	side_len_px = int(calib.getRawX(side_len))	
	numBoxY = int(Math.floor(h/side_len_px))
	numBoxX = int(Math.floor(w/side_len_px)) 
	if offset<0:
		remainX = (w - (numBoxX*side_len_px))/2 
		remainY = (h - (numBoxY*side_len_px))/2
		offset_string = 'centered'
	else:
		remainX =  int(calib.getRawX(offset))	
		remainY =  int(calib.getRawY(offset))
		offset_string = str(offset)
		
	
	for r in range(0,numBoxY):
		for c in range(0,numBoxX):			
			imp.setRoi(int(remainX + (c*side_len_px)), int(remainY+(r*side_len_px)), side_len_px, side_len_px)
			#IJ.run(imp, "Find Maxima...", "noise="+str(thr_max)+" output=[Point Selection]"); 
			roi = imp.getRoi()
			roi.setName(tit_img[0:len(tit_img)-4]+"_R"+str(r+1)+"_C"+str(c+1))	
			rm.addRoi(roi)
			save_path_folder = todo_crop(imp,roi,dstDir_path,side_len,offset_string)
			IJ.log('row: ' + str(r+1) + ' of ' + str(numBoxY)+' col: ' + str(c+1) + ' of '+str(numBoxX))

	#save the gridded image
	imp.setTitle(tit_img)
	rm.runCommand(imp,"Show All with labels");
	print save_path_folder 
	save_path = str(dstDir)+os.sep+imp.getTitle()+'_GRIDover_'+str(side_len)+'_offset_'+(offset_string)+'.tif'
	IJ.save(imp,save_path)
	IJ.log('DONE!')

if __name__ in ['__builtin__', '__main__']:
    run_script()