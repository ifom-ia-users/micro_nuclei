# README #

### What is this plugin for ###

* A Fiji plugin for semi automatic recognition of Micro Nuclei.
  It comes with an auxiliary plugin (*GridJ_.py*) in order to grid big images: for example those ones coming from big tiling acquisition.

### Setup ###

* Install [Fiji](https://fiji.sc/) 
* Copy the python .py files (in the [analysis subfolder](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/src/master/analysis/)) in the Fiji Plugin subfolder

### How it works: ###

* Launch Fiji

* Check if the plugins are in the plugin menu

![pluginlist](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/raw/master/screenshots/how-to/howto_pluginlist.jpg)

* *if you need to make a grid, for example if you have a big image*

  * Open an image

  * Launch **GridJ** from plugin menu, it will create a grid on your image and save every square in a folder as a crop

![gridj](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/raw/master/screenshots/how-to/howto_gridJ.jpg)

  * **Output directory**: where you want to save the cropped images

  * Set the **side length** of a square and if you want a starting **offset**

  * Check the result in the **output folder**    

    ![outputGrid](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/raw/master/screenshots/how-to/howto_GridJ_results.jpg)
    
* the cropped images are called Rx_Cy_nameofthefile
where R is Row, C is Column and x,y are the numbers of them
The ouput folder adds to its name the offset and side length used.

* You can now run the **Micro Nuclei** plugin using the cropped images contained in the output folder created by *GridJ*

* Launch **Micro Nuclei** from plugin menu

  * Select the crop directory and where you want to save the analysis

![micronuclei](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/raw/master/screenshots/how-to/howto_MicroNuclei_menu.jpg)

  * The plugin offers you the opportunity to do a completely **automatic analysis**, or check/add/delete micronuclei crop per crop **manually**

    * in this case, for every crop you can add or delete micronuclei using the RoiManager
    
    * the Micronuclei are found using Huang Threshold method after a median filter, the ImageJ Watershed algorithm is then used for micro nuclei splitting

  * The **Results** are 

    * a Results Table with the number of micronuclei in every cropped image (**REMEMBER to SAVE IT**  :grimacing:) 
    * a list of images with the micronuclei overlaid in the ouput folder (to view the overlaid micronuclei open the images with Fiji)

  * Please find also a [powerpoint presentation here](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/src/master/tutorial/), with a step by step tutorial

    

  

  

  

  

  

  ---

### Contacts: ###

* <imageanalysis-desk@ifom.eu>

### How to cite and share: ###

* *cite*:
  - ImageJ and Fiji:
    <https://imagej.net/Citing>
  - This website: <https://bitbucket.org/Ifom_IA_dev/micro_nuclei>
* *share*:
  - if yous share the code, don't forget the **license.txt** in the [license folder](https://bitbucket.org/Ifom_IA_dev/micro_nuclei/src/master/LICENSE/)

### Publications where it was used: ###

- by now none :sweat_smile:

